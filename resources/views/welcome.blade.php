<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro:wght@200;300&display=swap" rel="stylesheet">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">



        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Source Code Pro', sans-serif;
                font-weight: 300;
                height: 100vh;
                margin: 0;
            }
            

            .full-height {
                height: 50vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-top: 50px;
                margin-bottom: 30px;
            }
        </style>
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
    </head>
    <body>

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md ">
                    <form method="POST" action="/tasks">
                        @csrf
                        <input type="text" name="title" class="input_field" autocomplete="off">
                        <button type="submit" class="input_button">add task</button>
                    </form>
                </div>
                <div>
                    <h2 style="padding-top: 100px;padding-bottom: 20px"><span>Your Task list</span></h2>
                    <div style="position: absolute;">
                        <table class="demo">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($tasks as $task)
                                    <tr>
                                        @if($task->done)
                                            <del>{{ $task->title }}</del>
                                        @else
                                            <td>{{ $task->title }}</td>
                                        @endif       
                                        
                                        <td>
                                            <form method="POST" action="/tasks/{{$task->id}}">
                                                @csrf
                                                @method('PATCH')
                                                @if($task->done)
                                                    <button type="submit" class="done" style="margin-left: 50px;" disabled>Its done</button>
                                                @else
                                                    <button type="submit" class="done" style="margin-left: 50px;">done</button>
                                                @endif                                               
                                            </form>
                                        </td>
                                        <td>
                                            <form method="POST" action="/tasks/{{$task->id}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="remove">delete</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </body>
</html>
