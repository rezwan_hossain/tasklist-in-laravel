<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Auth;

class TaskController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    	$tasks =  Auth::user()->tasks;
    	return  view('welcome', compact('tasks'));
    }

    public function store()
    {
    		$task = new Task ;
    		$task->title = request('title');
    		$task->user_id = Auth::user()->id;

    		$task->save();
    		return redirect()->back(); 
    }

    public function destroy($id)
    {
    	$task = Task::findOrFail($id);
    	$task->delete();

    	return redirect()->back(); 
    }

    public function doneButton($id)
    {
    	$task = Task::findOrFail($id);

    	$task->done = !$task->done;
    	$task->save();

    	return redirect()->back();
    }
}
